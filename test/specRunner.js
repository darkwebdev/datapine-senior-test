/* global require */
'use strict';

require.config({
    baseUrl: '.',
    paths: {
        jquery: '../app/bower_components/jquery/jquery',
        backbone: '../app/bower_components/backbone/backbone',
        underscore: '../app/bower_components/lodash/lodash'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    }
});

require([
        'spec/test'
    ], function() {
        mocha.run();
    }
);
