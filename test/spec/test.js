/* global describe, it, define */
'use strict';

define(function(require) {
    var Backbone = require('backbone');
    var _ = require('underscore');
    var Ctrl = require('../../app/scripts/ctrl');
    var expect = chai.expect;

    mocha.setup('bdd');

    var fakeSeries = [
        [
            {
                name: 'Tokyo',
                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }, {
                name: 'New York',
                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            }
        ], [
            {
                name: 'Berlin',
                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
            }, {
                name: 'London',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }
        ]
    ];
    var fakeChartListView = {
        updateWith: sinon.spy()
    };
    var fakeChartDetailView = {
        render: sinon.spy(),
        hide: sinon.spy()
    };
    var fakeAboutView = {
        render: sinon.spy(),
        hide: sinon.spy()
    };
    var historyOptions = { trigger: true };

    describe('App', function () {

        describe('on run', function () {
            beforeEach(function() {
                Ctrl(fakeSeries, fakeChartListView, fakeChartDetailView, fakeAboutView);
            });
            it('should properly pass model data to the view', function() {
                var collection = fakeChartListView.updateWith.getCall(0).args[0];

                _.each(collection.models, function(chart, chartIndex) {
                    _.each(chart.attributes, function(city, cityIndex) {
                        expect(city.name).to.equal(fakeSeries[chartIndex][cityIndex].name);
                    });
                });
            });

            it('should hide all additional views', function() {
                Ctrl(fakeSeries, fakeChartListView, fakeChartDetailView, fakeAboutView);
                Backbone.history.start();

                Backbone.history.navigate('#', historyOptions);

                expect(fakeAboutView.hide).to.be.called;
                expect(fakeChartDetailView.hide).to.be.called;

                Backbone.history.stop();
            });
        });

        describe('on navigation', function () {
            beforeEach(function() {
                Ctrl(fakeSeries, fakeChartListView, fakeChartDetailView, fakeAboutView);
                Backbone.history.start();
            });
            afterEach(function() {
                Backbone.history.navigate('#');
                Backbone.history.stop();
            });
            it('should show selected page', function() {
                Backbone.history.navigate('#about', historyOptions);

                expect(fakeAboutView.render).to.be.called;
            });
            it('should pass proper Model Data to the Chart Detail View', function () {
                var chartIndex = 1;

                Backbone.history.navigate('#chart/' + chartIndex, historyOptions);

                expect(fakeChartDetailView.render).to.be.called;

                var chart = fakeChartDetailView.render.getCall(0).args[0].attributes;

                _.each(chart, function(model, i) {
                    expect(model).to.deep.equal(fakeSeries[chartIndex][i]);
                });
            });
        });
    });

});
