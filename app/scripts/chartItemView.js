define(function(require) {
    'use strict';

    require('highcharts');
    var Backbone = require('backbone');

    return Backbone.View.extend({
        tagName: 'a',

        render: function (options) {
            var defaults = {
                title: {
                    text: '',
                    x: -20
                },
                xAxis: {
                    categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
                },
                yAxis: {
                    title: {
                        text: 'Temperature (°C)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    enabled: false
                },
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'top',
                    borderWidth: 0
                },
                plotOptions: {
                    series: {
                        enableMouseTracking: false,
                        animation: false,
                        states: {
                            hover: {
                                enabled: false
                            }
                        },
                        marker: {
                            enabled: false
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: _.values(this.model.attributes)
            };

            this.$el.attr('href', '#chart/' + this.attributes.index);
            this.$el.highcharts(_.extend({}, defaults, options));

            return this;
        },

        showAsBars: function() {
            this.render({
                chart: {
                    type: 'column'
                }
            });

            return this;
        }
    });
});
