define([
    'backbone'
], function (Backbone) {
    'use strict';

    return function(series, chartListView, chartDetailView, aboutView) {

        var collection = new Backbone.Collection(series);

        chartListView.updateWith(collection);

        var hideViews = function() {
            chartDetailView.hide();
            aboutView.hide();
        };

        new (Backbone.Router.extend({
            routes: {
                'about': 'showAbout',
                'chart/:id': 'showChart',
                '*path': 'showChartList'
            },
            showChartList: function () {
                hideViews();
            },
            showAbout: function () {
                hideViews();

                aboutView.render();
            },
            showChart: function (id) {
                hideViews();

                chartDetailView.render(collection.models[id]);
            }
        }))();
    };
});
