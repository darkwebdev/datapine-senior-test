define(function(require) {
    'use strict';

    var Backbone = require('backbone');

    return Backbone.View.extend({
        el: '.about',
        render: function () {
            this.$el.show();

            return this;
        },
        hide: function() {
            this.$el.hide();

            return this;
        }
    });
});
