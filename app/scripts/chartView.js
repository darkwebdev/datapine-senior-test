define(function(require) {
    'use strict';

    require('highcharts');
    var Backbone = require('backbone');

    return Backbone.View.extend({
        el: '.chart-detail-container',
        render: function (model) {
            var options = {
                title: {
                    text: 'Monthly Average Temperature',
                    x: -20 //center
                },
                subtitle: {
                    text: 'Source: WorldClimate.com',
                    x: -20
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                yAxis: {
                    title: {
                        text: 'Temperature (°C)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    enabled: true,
                    valueSuffix: '°C'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: _.values(model.attributes)
            };

            this.$el.show().highcharts(options);

            return this;
        },
        hide: function() {
            this.$el.hide();

            return this;
        }
    });
});
