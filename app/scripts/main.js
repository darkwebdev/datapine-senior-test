/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        highcharts: {
            deps: ['jquery'],
            exports: 'Highcharts'
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        highcharts: '../bower_components/highcharts-release/highcharts',
        text: '../bower_components/requirejs-text/text'
    }
});

require([
    'backbone',
    'text!./series.json',
    './chartListView',
    './chartView',
    './aboutView',
    './ctrl'
], function (Backbone, series, ChartListView, ChartView, AboutView, ctrl) {

    ctrl(JSON.parse(series), new ChartListView(), new ChartView(), new AboutView());

    Backbone.history.start();

});
