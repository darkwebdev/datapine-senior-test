define([
    'backbone',
    './chartItemView'
], function (Backbone, ChartItemView) {
    'use strict';

    return Backbone.View.extend({
        el: '.chart-list-container',
        contentEl: '.chart-list-content',
        viewList: [],

        events: {
            'click .show-as-bars': 'showAsBars',
            'click .show-as-lines': 'render'
        },

        initialize: function() {
            _.bindAll(this, 'updateWith', 'render', 'showAsBars');
        },

        updateWith: function (collection) {
            this.viewList = collection.models.map(function (model, i) {
                return new ChartItemView({
                    model: model,
                    attributes: {
                        index: i
                    }
                });
            });

            this.render();

            return this;
        },

        render: function() {
            var $contentEl = this.$(this.contentEl);

            $contentEl.empty();

            this.viewList.forEach(function(view) {
                $contentEl.append(view.$el);
                view.render();
            });

            return this;
        },

        showAsBars: function() {
            _.invoke(this.viewList, 'showAsBars');

            return this;
        }
    });
});
